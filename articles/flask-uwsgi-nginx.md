Title: Setting up uWSGI + flask + nginx
Date: Feb 27, 2017
Draft: true

## uwsgi
> apt install uwsgi

### Registering uWSGI as a service using fucking systemd
The time goes by, but I still hate systemd since it replaced OpenRC in Gentoo in early 2010s. It is still unusable, shitty and totally non-UNIX. 

Execute as root:
> touch /etc/systemd/system/mdblog.service<br>
> chmod 664 /etc/systemd/system/mdblog.service

Create a dummy service configuration file like this:
>  # __/etc/systemd/system/mdblog.service__<br>
> [Unit]<br>
> Description=uWSGI service for markdown blog<br>
> After=network.target<br>
> 
> [Service]<br>
> WorkingDirectory=/var/www/mdblog/<br>
>  # Double check the uwsgi executable path with 'which uwsgi'<br>
> ExecStart=/usr/local/bin/uwsgi --ini mdblog.ini<br>
> Type=notify<br>
> PIDFile=/var/run/mdblog.pid<br>
> User=www-data<br>
> Group=www-data<br>
> 
> [Install]<br>
> WantedBy=default.target<br>

Starting service (_daemon-reload_ is crutial):
> systemctl daemon-reload && service mdblog start

Enabling service (i.e. making it to start up after reboot):
> systemctl enable mdblog.service

If there's any trouble, you may see the message like this:
> Job for mdblog.service failed because the control process exited with error code.
> See "systemctl status mdblog.service" and "journalctl -xe" for details.

In that case don't check _systemctl status_, cause it's fuckin useless, check the _/var/log/syslog_ instead, it's pretty much self-explaining there. E.g.:
> [/etc/systemd/system/mdblog.service:7] Executable path is not absolute, ignoring)

Check [RTFM](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/sect-Managing_Services_with_systemd-Unit_Files.html) if something went wrong.

## nginx
Installing nginx:
> apt install nginx

Starting nginx service:
> service nginx start

The default nginx welcome page must appear on [port 80](http://localhost:80)

Checking under which user the nginx is being run:
> ps aux | grep nginx

Checking in which groups this user is:
> groups www-data

Changing permissions to .sock file, so, that it can be accessed by nginx:
> chmod 660 


2017/02/27 15:53:07 [crit] 18441#18441: *1 connect() to unix:/home/tim/dev/myproject.sock failed (2: No such file or directory) while connecting to upstream, client: 127.0.0.1, server: server_domain_or_ip, request: "GET / HTTP/1.1", upstream: "uwsgi://unix:/home/tim/dev/myproject.sock:", host: "127.0.0.1"
2017/02/27 15:55:04 [crit] 18708#18708: *1 connect() to unix:/home/tim/dev/myproject/myproject.sock failed (13: Permission denied) while connecting to upstream, client: 127.0.0.1, server: server_domain_or_ip, request: "GET / HTTP/1.1", upstream: "uwsgi://unix:/home/tim/dev/myproject/myproject.sock:", host: "127.0.0.1"
2017/02/27 15:55:05 [crit] 18708#18708: *1 connect() to unix:/home/tim/dev/myproject/myproject.sock failed (13: Permission denied) while connecting to upstream, client: 127.0.0.1, server: server_domain_or_ip, request: "GET / HTTP/1.1", upstream: "uwsgi://unix:/home/tim/dev/myproject/myproject.sock:", host: "127.0.0.1"


