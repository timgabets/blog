# Tim Gabets

Hi, I'm [🔐timgabets](https://keybase.io/timgabets). I am a software developer, primarily working now with C/C++ and Oracle PL/SQL in the Fintech area, but I'm also still practicing some JavaScript and Python for my [personal projects](https://github.com/timgabets?tab=repositories). I'm currently employed by [BPC Banking Technologies](http://bpcbt.com) where I work as a card processing frontend developer.

In the past I worked for [TSYS](http://tsys.com) and [MyceliumSwish](http://swish.mycelium.com) as well as some other smaller projects, sometimes even not related to software engineering.

I earned my [Bachelor Degree in Computer Science](https://github.com/timgabets/thesis/blob/master/doc/thesis.pdf) in [Moscow Tech](http://mti.edu.ru/) in 2016. Also in the past I successfully completed some graduate courses at [Harvard Extension School](https://www.extension.harvard.edu/), which I'm still [very proud of](http://gabets.ru/CS61).

If you happen to have any questions or just would like to contact me, you can do this by [email](mailto:tim@gabets.ru). You can also find me on [Github](https://github.com/timgabets), [LinkedIn](https://www.linkedin.com/in/timgabets/) and [Deezer](http://www.deezer.com/profile/1105580242).

My GPG key is [CD5A97B2](../static/Tim Gabets.asc) registered using Email address tim@gabets.ru. Other keys registered on my name are no longer in use.
