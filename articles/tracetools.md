Title: python tracetools
Date: May 18, 2017

[python tracetools](https://pypi.python.org/pypi/tracetools) is a simple library to print formatted trace messages, which is especially useful while working with the binary data:

12:08:16.762628 >> 206 bytes sent:<br>
&nbsp;&nbsp;&nbsp;&nbsp;00 cc 30 31 30 30 72 30 05 80 20 c0 92 00 31 36&nbsp;&nbsp;&nbsp;&nbsp;..0100r0.. ...16<br>
&nbsp;&nbsp;&nbsp;&nbsp;38 39 39 30 30 31 31 32 33 34 35 36 37 38 39 30&nbsp;&nbsp;&nbsp;&nbsp;8990011234567890<br>
&nbsp;&nbsp;&nbsp;&nbsp;30 30 30 30 30 30 30 30 30 30 30 30 30 32 30 30&nbsp;&nbsp;&nbsp;&nbsp;0000000000000200<br>
&nbsp;&nbsp;&nbsp;&nbsp;30 30 31 34 39 34 35 30 34 34 39 34 38 30 30 32&nbsp;&nbsp;&nbsp;&nbsp;0014945044948002<br>
&nbsp;&nbsp;&nbsp;&nbsp;33 31 31 37 30 35 31 31 31 32 30 38 31 34 39 30&nbsp;&nbsp;&nbsp;&nbsp;3117051112081490<br>
&nbsp;&nbsp;&nbsp;&nbsp;30 31 30 30 30 30 33 32 38 39 39 30 30 31 31 32&nbsp;&nbsp;&nbsp;&nbsp;0100003289900112<br>
&nbsp;&nbsp;&nbsp;&nbsp;33 34 35 36 37 38 39 30 44 31 38 30 39 32 30 31&nbsp;&nbsp;&nbsp;&nbsp;34567890D1809201<br>
&nbsp;&nbsp;&nbsp;&nbsp;31 38 37 32 33 30 30 30 31 30 30 30 31 33 33 37&nbsp;&nbsp;&nbsp;&nbsp;1872300010001337<br>
&nbsp;&nbsp;&nbsp;&nbsp;39 39 39 39 39 39 39 39 39 39 39 39 30 30 31 36&nbsp;&nbsp;&nbsp;&nbsp;9999999999990016<br>
&nbsp;&nbsp;&nbsp;&nbsp;34 33 2b 68 7a ef c3 4b 1a 89 30 34 39 82 02 00&nbsp;&nbsp;&nbsp;&nbsp;43+hz..K..049...<br>
&nbsp;&nbsp;&nbsp;&nbsp;00 9a 03 17 05 11 95 05 00 00 04 08 80 9f 10 02&nbsp;&nbsp;&nbsp;&nbsp;................<br>
&nbsp;&nbsp;&nbsp;&nbsp;00 00 9f 26 08 ed 2c d0 d2 98 94 fb aa 9f 36 02&nbsp;&nbsp;&nbsp;&nbsp;...&..,.......6.<br>
&nbsp;&nbsp;&nbsp;&nbsp;00 01 9f 37 04 8f a4 f2 bd 9f 1a 02 06 43&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...7.........C<br>

12:08:17.013138 << 110 bytes received:<br> 
&nbsp;&nbsp;&nbsp;&nbsp;00 6c 30 31 31 30 72 30 00 00 0e 80 80 00 31 36&nbsp;&nbsp;&nbsp;&nbsp;.l0110r0......16<br>
&nbsp;&nbsp;&nbsp;&nbsp;38 39 39 30 30 31 31 32 33 34 35 36 37 38 39 30&nbsp;&nbsp;&nbsp;&nbsp;8990011234567890<br>
&nbsp;&nbsp;&nbsp;&nbsp;30 30 30 30 30 30 30 30 30 30 30 30 30 32 30 30&nbsp;&nbsp;&nbsp;&nbsp;0000000000000200<br>
&nbsp;&nbsp;&nbsp;&nbsp;30 30 31 34 39 34 35 30 34 34 39 34 38 30 30 32&nbsp;&nbsp;&nbsp;&nbsp;0014945044948002<br>
&nbsp;&nbsp;&nbsp;&nbsp;33 31 31 37 30 35 31 31 31 32 30 38 31 34 30 30&nbsp;&nbsp;&nbsp;&nbsp;3117051112081400<br>
&nbsp;&nbsp;&nbsp;&nbsp;30 30 30 30 31 30 36 38 34 38 31 30 36 38 34 38&nbsp;&nbsp;&nbsp;&nbsp;0000106848106848<br>
&nbsp;&nbsp;&nbsp;&nbsp;30 30 30 31 30 30 30 31 33 33 37 36 34 33&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;00010001337643<br>


Usage:<br>
 &gt;&gt;&gt;pip install tracetools<br>
 &gt;&gt;&gt;from tracetools.tracetools import trace<br>
 &gt;&gt;&gt;trace('user-defined-title(optional)', data)<br>

 &gt;&gt;&gt;from tracetools.tracetools import trace<br>
 &gt;&gt;&gt;trace(b'\x01\xFF17398172371892371982737981723')<br>
22:27:00.967302<br>
&nbsp;&nbsp;&nbsp;&nbsp;01 ff 31 37 33 39 38 31 37 32 33 37 31 38 39 32         ..17398172371892<br>
&nbsp;&nbsp;&nbsp;&nbsp;33 37 31 39 38 32 37 33 37 39 38 31 37 32 33            371982737981723<br>
	
 &gt;&gt;&gt;trace(title='Some stuff received:', data=b'\x00\x6c\x30\x31\x31\x30\x72\x30\x00\x00\x0e\x80\x80\x00\x31\x36')<br>
22:28:53.709328 Some stuff received:<br>
&nbsp;&nbsp;&nbsp;&nbsp;00 6c 30 31 31 30 72 30 00 00 0e 80 80 00 31 36         .l0110r0......16<br>
