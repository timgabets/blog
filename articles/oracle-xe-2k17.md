Title: Oracl XE Installation manual on a Linux desktop. A 2k17 guide
Date: Nov 16, 2017
Draft: True


Oracle Database Express Edition (aka XE) may be installed from the [Oracle website](http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html). 
The download will start automatically after signing in with your Oracle account and accepting the license agreement.


###Installation
The installation must be performed by root user.

 unzip oracle-xe-11.2.0-1.0.x86_64.rpm.zip
 cd Disk1

After extracting the archive, the Disk1 directory will be created with the oracle-xe-11.2.0-1.0.x86_64.rpm package inside. On a Red Hat/Fedora system it may be installed with
 rpm -i oracle-xe-11.2.0-1.0.x86_64.rpm

On Ubuntu/Debian:
 apt-get install alien
 alien --scripts oracle-xe-11.2.0-1.0.x86_64.rpm 
 dpkg -i oracle-xe_11.2.0-2_amd64.deb

Please keep in mind that the default installation path is /u01/app/oracle/product/11.2.0, therefore ensure that there is some free disk space in your / directory to fit your database files. Alternatively, use a separate ~100G partition for that:

 mount 
  /dev/sdb1 on /u01 type ext4 (rw,relatime,data=ordered)

 fdisk
  Device     Boot Start       End   Sectors  Size Id Type
  /dev/sdb1        2048 209715199 209713152  100G 83 Linux

###Configuration

Add the following Oracle environment variables to your shell rc-files (~/.bashrc for Bash, ~/.zshrc):
 export ORACLE_BASE=/u01/app/oracle/product/11.2.0
 export ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe
 export PATH=$PATH:$ORACLE_HOME/bin:$ORACLE_HOME/config/scripts/  
 
Re-read the shell config:
 source ~/.zshrc

Copy .ora sample files:
 cp $ORACLE_HOME/network/admin/samples/listener.ora $ORACLE_HOME/network/admin/listener.ora
 cp $ORACLE_HOME/network/admin/samples/tnsnames.ora $ORACLE_HOME/network/admin/tnsnames.ora

Then add the following data to these files:

 vim $ORACLE_HOME/network/admin/tnsnames.ora
 xe =
   (DESCRIPTION =
         (ADDRESS =
           (PROTOCOL = TCP)
           (Host = localhost )
           (Port = 1521)
         )
    (CONNECT_DATA = (SID = xe)
      (SERVER = DEDICATED)
      (SERVICE_NAME = xe)
    )
 )

 vim $ORACLE_HOME/network/admin/listener.ora
 LISTENER =
  (ADDRESS_LIST=
        (ADDRESS=
         (PROTOCOL=tcp)
         (HOST=localhost)
         (PORT=1521))
         )  

Finally, the initial confiuration may be perfomed by the script below:
 rm -f /etc/default/oracle-xe # to remove previous config files
 /etc/init.d/oracle-xe configure

The script is asking for a basic configuration settings such as SYS/SYSTEM password, listener port, APEX listening port etc. The default options are working fine in the most cases.


###User account setup
It's generally a good idea to start/stop your database as non-superuser (e.g. root or constanly using sudo).

As you may see, all the files in $ORACLE_HOME are belongs to _oracle_ user in _dba_ group:
 ls -lh $ORACLE_HOME/network/admin/listener.ora
 -rwxr-xr-x 1 oracle dba 3,9K nov 16 14:10 /u01/app/oracle/product/11.2.0/xe/network/admin/listener.ora

If you want to be able to edit Oracle XE configuration with your regular user, grant the $ORACLE_BASE write permissions to _dba_ group:
 # execute as a superuser, or use sudo:
 chmod g+w $ORACLE_BASE -R # grant the write permission to group

Then, add your _user_ to dba group:
 usermod -a -G dba user

You need to log out your _user_ to apply the changes (or just reboot). After the log in check the your groups:
 $ groups
 tim sudo dba

Now you should be able to edit any configuration in $ORACLE_HOME:
 $ vim $ORACLE_HOME/network/admin/tnsnames.ora


###Configuring listener
Try to start Oracle listener as a regular user:
 $ lsnrctl start
 LSNRCTL for Linux: Version 11.2.0.2.0 - Production on 16-NOV-2017 15:23:04
 Copyright (c) 1991, 2011, Oracle.  All rights reserved.
 Starting /u01/app/oracle/product/11.2.0/xe/bin/tnslsnr: please wait...
 TNSLSNR for Linux: Version 11.2.0.2.0 - Production
 System parameter file is /u01/app/oracle/product/11.2.0/xe/network/admin/listener.ora
 Log messages written to /u01/app/oracle/product/11.2.0/diag/tnslsnr/svfe/listener/alert/log.xml
 Listening on: (DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=127.0.0.1)(PORT=1521)))
 Connecting to (ADDRESS=(PROTOCOL=tcp)(HOST=localhost)(PORT=1521))
 STATUS of the LISTENER
 ------------------------
 Alias                     LISTENER
 Version                   TNSLSNR for Linux: Version 11.2.0.2.0 - Production
 Start Date                16-NOV-2017 15:23:04
 Uptime                    0 days 0 hr. 0 min. 0 sec
 Trace Level               off
 Security                  ON: Local OS Authentication
 SNMP                      OFF
 Listener Parameter File   /u01/app/oracle/product/11.2.0/xe/network/admin/listener.ora
 Listener Log File         /u01/app/oracle/product/11.2.0/diag/tnslsnr/svfe/listener/alert/log.xml
 Listening Endpoints Summary...
   (DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=127.0.0.1)(PORT=1521)))
 The listener supports no services
 The command completed successfully

Check the mentioned above files/directories to confirm that they exist. For example, although it was mentioned that the log data was written to /u01/app/oracle/product/11.2.0/diag/tnslsnr/svfe/listener/alert/log.xml, the file did not exist, and I needed to create the directories by myself:
 # as root:
 mkdir -p $ORACLE_BASE/diag/tnslsnr/svfe/listener/alert
 chown oracle:dba $ORACLE_BASE/ -R
 chmod g+w $ORACLE_BASE/ -R

Finally, looks like the listener is started normally, and we may now check it with _tnsping_:
 $ tnsping xe
 Used TNSNAMES adapter to resolve the alias
   Attempting to contact (DESCRIPTION = (ADDRESS = (PROTOCOL = TCP) (Host = localhost) (Port = 1521)) (CONNECT_DATA = (SID = xe) (SERVER = DEDICATED) (SERVICE_NAME = xe)))
   OK (0 msec)


