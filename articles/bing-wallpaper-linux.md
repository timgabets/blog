Title: Using Bing Wallpapers in Lunux
Date: Jan 31, 2017

Just written [a simple python script](https://github.com/timgabets/Bing-Wallpaper-Fetch) to get fancy [Bing Wallpapers](http://www.bing.com/gallery/) on my Linux desktop. Usage:

>python3 bing_wallpaper_fetch.py -c 8 -d ~/Pictures/Wallpapers

The script is added to /etc/crontab to fetch the latest wallpaper every day:

>  #/etc/crontab<br>
>  #Fetch the new Bing Wallpaper daily at 13:00<br>
>0 13 * * * tim python3 /home/tim/dev/bing-wallpaper-fetch/bing_wallpaper_fetch.py -c 1 -d ~/Pictures/Wallpapers/Bing<br>

After being fetched, the images are picked up and displayed by amazing [vrty application](http://peterlevi.com/variety/).

_Note: due to the Bing’s API limitation, only the last 16 images are available for download – to be fixed later_
